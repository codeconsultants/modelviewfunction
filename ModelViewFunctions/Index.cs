using System;
using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using Markdig;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Azure.WebJobs.Host;
using Newtonsoft.Json;

using RazorEngine;
using RazorEngine.Compilation;
using RazorEngine.Compilation.ReferenceResolver;
using RazorEngine.Configuration;
using RazorEngine.Roslyn;
using RazorEngine.Templating;
using RazorEngine.Text;
using RazorLight;
using IActivator = RazorEngine.Templating.IActivator;
using ModelViewFunctions.Extensions;
using RazorLight.Templating;
using ITemplateManager = RazorEngine.Templating.ITemplateManager;
using ITemplateSource = RazorLight.Templating.ITemplateSource;

namespace ModelViewFunctions
{
    public static class Index
    {
        private class RazorConfig : ITemplateServiceConfiguration
        {
            public IActivator Activator { get; }
            public bool AllowMissingPropertiesOnDynamic => true;
            public Type BaseTemplateType { get; }
            public IReferenceResolver ReferenceResolver { get; }
            public ICachingProvider CachingProvider { get; }
            public ICompilerServiceFactory CompilerServiceFactory { get; }
#if DEBUG
            public bool Debug => true;
#else
            public bool Debug => false;
#endif
            public bool DisableTempFileLocking => false;
            public IEncodedStringFactory EncodedStringFactory { get; }
            public Language Language => Language.CSharp;
            public ISet<string> Namespaces { get; }
            public ITemplateResolver Resolver { get; }
            public ITemplateManager TemplateManager { get; }

            public RazorConfig(params string[] namespaces)
            {
                Activator = new RazorActivator();
                CachingProvider = new DefaultCachingProvider();
                //CompilerServiceFactory = new DefaultCompilerServiceFactory(); // Untested
                CompilerServiceFactory = new RoslynCompilerServiceFactory();
                //EncodedStringFactory = new HtmlEncodedStringFactory(); // Untested
                EncodedStringFactory = new RawStringFactory();
                Namespaces = new HashSet<string>(namespaces);
                ReferenceResolver = new UseCurrentAssembliesReferenceResolver();
                // Template manager and resolver
                TemplateManager = new EmbeddedResourceTemplateManager(typeof(Index));
            }

            private class RazorActivator : IActivator
            {
                // NB: Copied from (internal) DefaultActivator
                public ITemplate CreateInstance(InstanceContext context)
                {
                    if (context == null)
                        throw new NullReferenceException(nameof(context));

                    return context.Loader.CreateInstance(context.TemplateType);
                }
            }

        }

        [FunctionName("Index")]
        public static IActionResult Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get" /*, "post"*/, Route = null)] HttpRequest req,
            TraceWriter log)
        {
            log.Info("C# HTTP trigger function processed a request.");

            string name = req.Query["name"];

            string requestBody = new StreamReader(req.Body).ReadToEnd();
            dynamic data = JsonConvert.DeserializeObject(requestBody);
            name = name ?? data?.name;

            //var razor = Engine.IsolatedRazor;
            //var razor = Engine.Razor;
            /*
            var razor= new RazorEngineService(new TemplateServiceConfiguration
            {
                AllowMissingPropertiesOnDynamic = true,
                Language = Language.CSharp,
                Debug = true,
                Namespaces = new HashSet<string> { "System", "System.Web", "System.Web.Mvc", "ModelViewFunctions.Models" }
            }){};
            */
            //var razor = new RazorEngineService(Language.CSharp, Encoding.Raw);
            // var info = typeof(RazorEngineService).FullName; // OK
            /*
            var razorConfig = new TemplateServiceConfiguration
            {
                AllowMissingPropertiesOnDynamic = true,
                Language = Language.CSharp,
                Debug = true,
                Namespaces = new HashSet<string> { "System", "System.Web", "System.Web.Mvc", "ModelViewFunctions.Models", "ModelViewFunctions.Extensions" }
            };
            */
            var razorConfig = new RazorConfig();
            //var razorConfig = new RazorConfig("System", "System.Web", "System.Web.Mvc", "ModelViewFunctions.Models", "ModelViewFunctions.Extensions");
            //throw new Exception("PREBUG");
            var razor = RazorEngineService.Create(razorConfig);
            //throw new Exception("DEBUG 2");

            //var info = "Missing";
            //var info = $"{razor.GetType().Assembly.FullName}";
            //var info = razor.RunCompile("Views.Index");  // FAILS: 

            // Razor Light (2)
            /*
            //var razorLight = EngineFactory.CreateEmbedded(typeof(Index)); // Namespace issue
            var templateManager = new CustomTemplateManager();
            templateManager.Add("Views.Index", "<h1>Example</h1>");
            var razorLight = EngineFactory.CreateCustom(templateManager);

            //var template = "<h1>Example<h1>";
            //var template = "Views/Index";
            var template = "Views.Index";

            var model = new {Name = name};
            razorLight.Configuration.Namespaces.Add("System", "System.Web", "System.Web.Mvc",
                "ModelViewFunctions.Models", "ModelViewFunctions.Extensions", "RazorLight");

            var info = razorLight.Parse(template, model);
            */

            /* RazorDark attempt for Core 2.0
            var razorDark = new RazorDark.EngineFactory().ForEmbeddedResources(typeof(Index));
            var template = "Views.Index";
            var model = new {Name = name};
            dynamic viewBag = new DynamicViewBag();
            viewBag.Name = name;
            var info = razorDark.CompileRenderAsync(template, model, model.GetType(), viewBag as ExpandoObject).Result;
            */

            // ***************** Markdown attempts
            //var info = string.Join(", \r\n", typeof(Index).Assembly.GetManifestResourceNames());
            var type = typeof(Index);
            //var path = $"{type.AssemblyQualifiedName}.Views.Index.md";
            var path = $"{type.Assembly.GetName().Name}.Views.Index.md";
            var md = type.GetResourceAsString(path, true);
            //var info = md;
            var info = md != null 
                ? Markdown.ToHtml(md) 
                : $"Resource not found: {path}";

            return name != null
                //? new OkObjectResult($"Hello, {name} ... razorEngineInfo = {info}") as ActionResult
                ? info.Contains("<") ? new ContentResult{Content=info, ContentType="text/html"} : new OkObjectResult($"Hello, {name} ... razorEngineInfo = {info}") as ActionResult
                : new BadRequestObjectResult("Please pass a name on the query string or in the request body");
        }

        private class CustomTemplateManager : Dictionary<string, string>, RazorLight.Templating.ITemplateManager
        {
            public ITemplateSource Resolve(string key)
                => new RazorLight.Templating.LoadedTemplateSource(base[key]);
        }
    }
}
