﻿using System.Collections.Generic;

namespace ModelViewFunctions.Extensions
{
    public static class StringExtensions
    {
        public static TSet Add<TSet>(this TSet stringSet, params string[] additionalStrings)
            where TSet : ISet<string>
        {
            foreach (var additionalString in additionalStrings)
                stringSet.Add(additionalString);
            return stringSet;
        }
    }
}
