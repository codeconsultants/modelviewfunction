﻿using System;
using System.IO;
using System.Reflection;

namespace ModelViewFunctions.Extensions
{
    public static class EmbeddedResourceExtensions
    {
        public static string GetResourceAsString(this Assembly assembly, string name, bool throwExceptionIfMissing = false)
        {
            using (var stream = assembly.GetManifestResourceStream(name))
            {
                if(stream == null)
                    if(throwExceptionIfMissing)
                        throw new Exception($"Requested resource not found: {name}"
#if DEBUG
                            + $" ... names = [{string.Join(", ", assembly.GetManifestResourceNames())}]"
#endif
                            );
                using (var reader = new StreamReader(stream))
                {
                    return reader.ReadToEnd();
                }
            }
        }

        public static byte[] GetResourceAsBinaryArray(this Assembly assembly, string name,
            bool throwExceptionIfMissing = false)
        {
            using (var stream = assembly.GetManifestResourceStream(name))
            {
                if (stream == null)
                    if (throwExceptionIfMissing)
                        throw new Exception($"Requested resource not found: {name}"
#if DEBUG
                                            + $" ... names = [{string.Join(", ", assembly.GetManifestResourceNames())}]"
#endif
                        );
                var memoryStream = stream as MemoryStream;
                if (memoryStream == null)
                {
                    memoryStream = new MemoryStream();
                    stream.CopyTo(memoryStream);
                    memoryStream.Seek(0, SeekOrigin.Begin);
                };
                var byteArray = memoryStream.ToArray();
                memoryStream.Dispose();
                return byteArray;
            }
        }

        public static string GetResourceAsString(this Type type, string name, bool throwExceptionIfMissing = false)
            => type.Assembly.GetResourceAsString(name, throwExceptionIfMissing);

        public static byte[] GetResourceAsBinaryArray(this Type type, string name, bool throwExceptionIfMissing = false)
            => type.Assembly.GetResourceAsBinaryArray(name, throwExceptionIfMissing);
    }
}
