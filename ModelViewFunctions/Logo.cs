﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;
using ModelViewFunctions.Extensions;

namespace ModelViewFunctions
{
    public static class Logo
    {
        [FunctionName("Logo")]
        public static IActionResult Run(
                [HttpTrigger(AuthorizationLevel.Anonymous, "get" /*, "post"*/, Route = null)] HttpRequest req,
                TraceWriter log)
            /*
            => new FileContentResult(
                typeof(Logo).GetResourceAsBinaryArray($"{typeof(Logo).Assembly.GetName().Name}.Content.BlackSmall.png")
                , "image/png");
            */
        {
            var data = typeof(Logo).GetResourceAsBinaryArray(
                $"{typeof(Logo).Assembly.GetName().Name}.Content.BlackSmall.png");
            var result = new FileContentResult(data, "image/png");
            return result;
        }
    }
}
